import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChatserverService {
  public messages: object[] = [];
  private usersList:string[] = ["Василий", "Петр", "Вячеслав", "Илья", "Петр"];
  userObj: any;
  
  private messagesChanges: BehaviorSubject<any> = new BehaviorSubject([]);
  private usersListChanges: BehaviorSubject<any> = new BehaviorSubject([]);

  constructor() {
    this.messagesChanges.subscribe((value) => {
      this.messages = value;
    });
    this.usersListChanges.next(this.usersList) // set default users list
  }
  
  add(message: any) {
    this.messages.push(message);
  }

  addUser(username: string) {
    this.usersList.push(username);
  }

  getMessages(): Observable<any> {
    return this.messagesChanges.asObservable();
  }
  
  getUsers(): Observable<string> {
    return this.usersListChanges.asObservable();
  }

  deleteMessages(userType) {
    for (let i = 0; i < this.messages.length; i++) {
      this.userObj = this.messages[i];
      if (userType == this.userObj.user) {
        this.messages.splice(i, 1); // проверяем, кто отправил сообщение, если нужный нам человек, то удаляем его сообщение
        i--;
      }  
    }
  }

}
