import { Component, OnInit, Input } from '@angular/core';
import { ChatserverService } from '../chatserver.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
  @Input() message: string;
  public userType: string;
  public user:string;
  public sendData: Object;
  public result : any;
  public usersList: any;

  constructor(private ChatserverService: ChatserverService) { }

  public sendMessage() {
   this.sendData = {
     user: this.userType,
     message: this.message
   }
    this.ChatserverService.add(this.sendData)
  }

  public deleteMes(userType) {
    this.ChatserverService.deleteMessages(userType);
  }
  
  public changeUser(user){ // для изменения пользователя 
    console.log(user)
  }

  ngOnInit() {
    this.ChatserverService.getMessages().subscribe(
      result => this.result = result
    );
    this.ChatserverService.getUsers().subscribe(
      usersList => this.usersList = usersList
    );
  }

}
