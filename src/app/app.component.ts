import { Component } from '@angular/core';
import { ChatserverService } from './chatserver.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  result:any;
  
  constructor(private ChatserverService: ChatserverService) { }

  addUser(user:string) {
    //this.ChatserverService.addUser(user);
    // add user here
  }
  deleteUser(user:string) {
    // delete user here
  }
  ngOnInit() {
    this.ChatserverService.getUsers().subscribe(
      result => { this.result = result }
    )
  }
}
