import { TestBed, inject } from '@angular/core/testing';

import { ChatserverService } from './chatserver.service';

describe('ChatserverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChatserverService]
    });
  });

  it('should be created', inject([ChatserverService], (service: ChatserverService) => {
    expect(service).toBeTruthy();
  }));
});
